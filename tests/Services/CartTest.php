<?php

namespace App\Tests\Services;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use PHPUnit\Framework\TestCase;
use App\Services\Cart;

class CartTest extends TestCase
{
    
    public function testOffsetSet()
    {
        $session = new Session(new MockArraySessionStorage());
        $cart = new Cart($session);
        
        $cart[11] = 22;
        
        $this->assertEquals('{"11":22}', $session->get(Cart::SESSION_KEY));
        
        $cart[11] = 0;
        
        $this->assertEquals('[]', $session->get(Cart::SESSION_KEY));
        
        $exception = false;
        try {
            $cart[11] = -1;
        } catch (\RangeException $ex) {
            $exception = true;
        }
        
        $this->assertTrue($exception);
    }
    
    public function testOffsetUnset()
    {
        $session = new Session(new MockArraySessionStorage());
        $session->set(Cart::SESSION_KEY, '{"11":22}');
        $cart = new Cart($session);
        unset($cart[11]);
        
        $this->assertEquals('[]', $session->get(Cart::SESSION_KEY));
    }
}
