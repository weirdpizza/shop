# BoringPizza shop

## Requirements

* php 7.4
    * take a look at `Dockerfile` for a complete extension list
* composer
* nodejs + yarn or npm
* mysql or mariadb
* optional: docker + docker-compose

## Development

Copy `.env` to `.env.local` and set database credentials. 

```bash
    composer install
    yarn install
    yarn encore dev # --watch
```

And then run php dev server

```bash
    php -S localhost:8000 -t puiblic/
    # OR
    docker-compose up -d
```

Note that included `Dockerfile` is meant to be used for development not for prod.
Don't forget to update db schema with

```bash
    bin/console doctrine:migration:migrate
```

## Testing

[Symfony: Testing](https://symfony.com/doc/current/testing.html)

The project comes with `phpunit-bridge` so everything will be installed upon
the first run of `bin/phpunit`. Database fixtures may be loaded with command

```bash
    bin/console doctrine:fixture:load
```


## Deployment

[How to deploy Symfony application](https://symfony.com/doc/current/deployment.html)

```bash
    composer install --no-dev --optimize-autoloader
    composer dump-env prod --empty
    yarn install
    yarn encore prod
    APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear
```

