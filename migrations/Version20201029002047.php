<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201029002047 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE INDEX currency_name_idx ON currency (name)');
        $this->addSql('CREATE INDEX default_address_idx ON delivery_address (is_default)');
        $this->addSql('CREATE INDEX product_active_idx ON product (active)');
        $this->addSql('CREATE INDEX product_rank_idx ON product (rank)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX currency_name_idx ON currency');
        $this->addSql('DROP INDEX default_address_idx ON delivery_address');
        $this->addSql('DROP INDEX product_active_idx ON product');
        $this->addSql('DROP INDEX product_rank_idx ON product');
    }
}
