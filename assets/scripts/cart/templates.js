import {div, button, input, moneyFormat} from '../template';

export function productTpl(product, qty) {
    const root = div('cart-product');

    const qtySection = div('cart-item-qty');
    qtySection.append(
        button('btn-secondary cart-dec-btn', 'minus'),
        input('cart-qty-input', qty, true),
        button('btn-secondary cart-inc-btn', 'plus'),
        button('btn-link cart-trash-btn', 'trash')
    );

    root.append(
        div('cart-product-title', product.name),
        div('cart-product-price', moneyFormat.format(product.cartPrice)),
        qtySection
    );
    
    return root;
}
