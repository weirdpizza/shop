import ProductView from './ProductView';
import {$$, on} from '../common';
import {moneyFormat} from '../template';
import client from './client';

class CartView {
    
    constructor(products) {
        this.$el = $$('cart');
        this.widgets = {
            totalCost: this.$el.querySelector('.cart-money'),
            products: {
                $el: this.$el.querySelector('.cart-items'),
                children: {}
            }
        };
        
        Object.values(products).forEach((item) => {
            const productEl = this.$el
                    .querySelector(`.cart-product[data-id="${item.product.id}"]`);
            this.addProduct(item, productEl);
        });
        
    }
    
    async createProduct(item, productEl, isNew = false) {
        if(isNew) {
            item = await client.addItem(item.product.id);
        }
        
        const view = new ProductView(item, productEl);

        on(view, 'change', () => this.updateCost());
        on(view, 'remove', () => this.removeProduct(item.product.id));
        
        return view;
    }
    
    async addProduct(item, productEl, isNew = false) {
        let view = this.widgets.products.children[item.product.id];

        if(!view) {
            view = await this.createProduct(item, productEl, isNew);
            this.widgets.products.children[item.product.id] = view;
            this.widgets.products.$el.appendChild(view.$el);
        } else {
            view.addOne();
        }
        
        this.updateCost();
    }

    removeProduct(id) {
        this.widgets.products.children[id].$el.remove();
        delete this.widgets.products.children[id];
        this.updateCost();
    }
    
    updateCost() {
        const cost = Object.values(this.widgets.products.children)
            .reduce(
                (acc, product) => acc += product.data.price * product.data.qty,
                0
            );

        this.widgets.totalCost.innerText = moneyFormat.format(cost);
    }
    
}

export default CartView;
