import {moneyFormat} from '../template';
import {productTpl} from './templates';
import {on} from '../common';
import client from './client';

class ProductView extends EventTarget {
    
    emit(event, data) {
        const customEvent = new CustomEvent(event, data);
        this.dispatchEvent(customEvent);
    }
    
    setData(data, attr, value) {
        switch(attr) {
            case 'title':
                this.widgets.title.innerText = value;
                data.product.name = value;
                break;
            case 'price':
                this.widgets.price.innerText = moneyFormat.format(value);
                data.product.cartPrice = value;
                break;
            case 'qty':
                this.widgets.qty.value = value;
                data.qty = value;
                client.updateQty(data.product.id, value);
                this.emit('change', {qty: this.data.qty});
                break;
            default:
                return false;
        }
        
        return true;
    }
    
    getData(data, attr) {
        switch(attr) {
            case 'qty':
                return data.qty;
            case 'price':
                return data.product.cartPrice;
            default:
                return data.product[attr];
        }
    }
    
    addOne() {
        this.data.qty < 10 && this.data.qty++;
    }
    
    bindHandlers() {
        on(this.widgets.incBtn, 'click', () => {
            this.addOne();
        });
        
        on(this.widgets.decBtn, 'click', () => {
            this.data.qty > 1 && this.data.qty--;
        });
        
        on(this.widgets.removeBtn, 'click', () => {
            this.data.qty = 0;
            
            this.emit('remove');
        });
    }
    
    constructor(data, $el) {
        super();

        this.$el = $el || productTpl(data.product, data.qty);
        
        this.widgets = {
            title: this.$el.querySelector('.cart-product-title'),
            price: this.$el.querySelector('.cart-product-price'),
            qty: this.$el.querySelector('.cart-qty-input'),
            incBtn: this.$el.querySelector('.cart-inc-btn'),
            decBtn: this.$el.querySelector('.cart-dec-btn'),
            removeBtn: this.$el.querySelector('.cart-trash-btn')
        };
        
        this.data = new Proxy(data, {
            set: (...args) => this.setData(...args),
            get: this.getData
        });
        
        this.bindHandlers();
    }
    
}

export default ProductView;
