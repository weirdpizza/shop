import {$, $$, on, deligate, onDomReady, ajax} from '../common';

const UPDATE_THRESHOLD = 500;

class CartClient {
    
    constructor() {
        this.timeouts = {};
    }
    
    /**
     * 
     * @param {Number} id
     * @param {Number} qty
     * @returns {void}
     */
    updateQty(id, qty) {
        if(this.timeouts[id]) {
            clearTimeout(this.timeouts[id]);
        }

        this.timeouts[id] = setTimeout(() => {
            ajax('/cart', 'PATCH', `id=${id}&qty=${qty}`);
            this.timeouts[id] = null;
        }, UPDATE_THRESHOLD);
    }
    
    /**
     * 
     * @param {Number} id
     * @returns {undefined}
     */
    removeItem(id) {
        if(this.timeouts[id]) {
            clearTimeout(this.timeouts[id]);
        }

        ajax('/cart', 'PATCH', `id=${id}&qty=0`);
    }
    
    /**
     * 
     * @param {Number} id
     * @returns {Promise}
     */
    addItem(id) {
        return ajax('/cart', 'POST', `id=${id}`);
    }

}

export default new CartClient();
