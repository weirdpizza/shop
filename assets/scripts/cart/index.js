import {onDomReady, ajax, $, $$, on, deligate} from '../common';
import {moneyFormat} from '../template';
import CartView from './CartView';

onDomReady(async function () {
    const cart = $$('cart');
    const cartBtn = $('.cart-amount');
    const cartDropdown = $('.cart-dropdown');
    
    if(!cart) {
        return;
    }
    
    const {products, currency} = await ajax('/cart');
    moneyFormat.init(currency);
    
    const cartView = new CartView(products);
    

    // show/hide cart dropdown
    on(cartBtn, 'click', function (event) {
        event.stopPropagation();
        cartDropdown.classList.toggle('active');
    });
    
    // hide cart dropdown on clicks outside the block, but allow them inside
    on(cartDropdown, 'click', function (event) {
        event.stopPropagation();
    });
    
    on(document, 'click', function () {
        cartDropdown.classList.remove('active');
    });


    const cardsSection = $('.cards');
    
    deligate(cardsSection, '.cart-add-btn', 'click', function() {
        const parent = this.closest('.card');
        const productId = parseInt(parent.dataset.id);
        const icon = this.querySelector('.fas');
        const request = {
            product: {id: productId},
            qty: 1
        };
        
        this.disabled = true;
        icon.classList.remove('fa-pizza-slice');
        icon.classList.add('fa-sync', 'fa-spin');
        
        cartView
            .addProduct(request, null, true)
            .finally(() => {
                this.disabled = false;
                icon.classList.add('fa-pizza-slice');
                icon.classList.remove('fa-sync', 'fa-spin');
            });
    });

});