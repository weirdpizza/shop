import {onDomReady, $, $$, deligate, on} from './common';
import {moneyFormat} from './template';

onDomReady(function () {
    const checkoutTable = $('.checkout-table');
    
    if(!checkoutTable) {
        return;
    }
    
    const currency = checkoutTable.dataset.currency;
    moneyFormat.init(currency);
    
    const addressList = $('.delivery-address-list');
    const checkedAddress = addressList.querySelector('.delivery-address-radio:checked');
    const addresses = addressList.querySelectorAll('.delivery-address-radio');
    
    if(!checkedAddress && addresses.length > 0) {
        addresses[0].checked = true;
    }
    
    const checkoutForm = $$('checkout-form');
    
    on(checkoutForm, 'submit', function (event) {
        const addressInput = $('.delivery-address-input');
        const newAddress = $$('checkout-new-address');
        
        if((!addresses.length || newAddress.checked) && addressInput.value.trim().length === 0) {
            event.preventDefault();
            addressInput.focus();
        }
    });
    
    function recalculate() {
        let cost = 0;
        checkoutTable.querySelectorAll('.checkout-item').forEach(function (item) {
            const priceItem = item.querySelector('.checkout-item-price');
            const price = parseFloat(priceItem.dataset.price);
            const qtyInput = item.querySelector('.cart-qty-input');
            const qty = parseInt(qtyInput.value);
            cost += price * qty;
        });
        
        const deliveryFeeItem = $$('checkout-delivery-fee');
        const deliveryFee = parseFloat(deliveryFeeItem.dataset.price);
        
        $$('checkout-sum').innerText = moneyFormat.format(cost);
        $$('checkout-total').innerText = moneyFormat.format(cost + deliveryFee);
    }
    
    deligate(checkoutTable, '.cart-dec-btn', 'click', function() {
        const parent = this.closest('.cart-item-qty');
        const input = parent.querySelector('.cart-qty-input');
        const qty = parseInt(input.value);

        if(qty > 1) {
            input.value = qty - 1;
        }
        recalculate();
    });
    
    deligate(checkoutTable, '.cart-inc-btn', 'click', function() {
        const parent = this.closest('.cart-item-qty');
        const input = parent.querySelector('.cart-qty-input');
        const qty = parseInt(input.value);
        
        if(qty < 10) {
            input.value = qty + 1;
        }
        recalculate();
    });
    
    deligate(checkoutTable, '.checkout-remove-btn', 'click', function() {
        const parent = this.closest('.checkout-item');
        parent.remove();
        recalculate();
    });
    

});
