/**
 * Get html element by its css-selector
 * 
 * @param {String} el
 * @returns {Element}
 */
export const $ = (el) => document.querySelector(el);

/**
 * Get html element by its id
 * 
 * @param {String} el
 * @returns {Element}
 */
export const $$ = (el) => document.getElementById(el);

/**
 * Add event listener
 * 
 * @param {Element} el
 * @param {String} event
 * @param {Function} handler
 */
export const on = (el, event, handler) => el.addEventListener(event, handler);

/**
 * Delegate event listener to a upper level element
 * 
 * @param {Element} parent
 * @param {String} el
 * @param {String} event
 * @param {Function} handler
 */
export function deligate(parent, el, event, handler) {
    on(parent, event, function (event) {
        const $el = event.target;
        if($el.matches(el)) {
            handler.call($el, event);
        /* 
         * FIXME: actually it should go all the way up to 
         * the parent node recoursively but only one node is enough for now
         */
        } else if ($el.parentNode.matches(el)) {
            handler.call($el.parentNode, event);
        }
    });
}

/**
 * DOMContentLoaded handler shorthand
 * 
 * @param {Function} handler
 */
export const onDomReady = (handler) => on(document, 'DOMContentLoaded', handler);

/**
 * 
 * @param {String} addr
 * @param {String} method
 * @param {Object} body
 * @returns {Promise}
 */
export async function ajax(addr, method, body) {
    const response = await fetch(addr, {
        method, 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            "X-Requested-With": "XMLHttpRequest"
        }, 
        body
    });

    return response.json();
}
