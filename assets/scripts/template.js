class MoneyFormatter {
    
    init(currency, locale = 'en-US') {
        this.formatter = new Intl.NumberFormat(locale, {
            style: 'currency',
            currency
        }); 
    }
    
    format(value) {
        return this.formatter.format(value);
    }
    
}

export const moneyFormat = new MoneyFormatter();

export function div(cssClass, text = '') {
    const el = document.createElement('div');
    el.innerText = text;
    el.className = cssClass;
    
    return el;
}

export function button(cssClass, iconClass) {
    const el = document.createElement('button');
    el.className = 'btn ' + cssClass;
    const icon = document.createElement('span');
    icon.className = 'fas fa-' + iconClass;
    el.appendChild(icon);
    return el;
}

export function input(className, value, readonly = false) {
    const el = document.createElement('input');
    el.type = 'text';
    el.className = 'form-control ' + className;
    el.value = value;
    el.readonly = readonly;
    return el;
}
