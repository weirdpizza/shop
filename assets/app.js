import "core-js/stable";
import '@fortawesome/fontawesome-free/css/all.css'
import './styles/app.css';
import './styles/header.css';
import './styles/footer.css';
import './styles/controls.css'
import './styles/cards.css';
import './styles/pages/products.css';
import './styles/pages/cart.css';
import './styles/pages/checkout.css';

import './scripts/cart';
import './scripts/checkout';
