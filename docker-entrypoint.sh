#!/bin/sh

php bin/console doctrine:migration:migrate -q
php bin/console doctrine:fixture:load -q
php -S 0.0.0.0:8000 -t public
