<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProductRepository;
use App\Services\CurrencyService;

/**
 * @Route("/")
 */
class DefaultController extends AbstractController
{
    
    /**
     * @Route("/", name="default")
     */
    public function index(ProductRepository $repo, CurrencyService $currency)
    {
        $products = $repo->findActive();
        
        return $this->render('default/index.html.twig', [
            'products' => $products,
            'currency' => $currency->getCurrency()->getName()
        ]);
    }
    
    /**
     * @Route("/currency", name="currency")
     */
    public function currency(Request $request, SessionInterface $session)
    {
        $currency = $request->get('c');
        $allowed = $this->getParameter('app.allowed_currency');
        
        if(in_array($currency, $allowed)) {
            $session->set('currency', $currency);
        }
        
        return $this->redirectToRoute('default');
    }
    
}
