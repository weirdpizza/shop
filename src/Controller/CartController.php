<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\{JsonResponse, Response, Request};
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

use App\Repository\ProductRepository;
use App\Entity\Product;
use App\Services\{Cart, CartConverter, CurrencyService};

/**
 * @Route("/cart")
 */
class CartController extends AbstractController
{

    private ProductRepository $productsRepo;
    
    private Cart $cart;
    
    private CurrencyService $currency;
    
    public function __construct(
        ProductRepository $repo,
        Cart $cart,
        CurrencyService $currency
    )
    {
        $this->productsRepo = $repo;
        $this->cart = $cart;
        $this->currency = $currency;
    }
    
    /**
     * 
     * @return Response
     */
    public function widget(CartConverter $converter): Response
    {   
        $currency = $this->currency->getCurrency();
        return $this->render('default/cart.html.twig', [
            'sum' => $converter->calculateSum() * $currency->getRate(),
            'currency' => $this->currency->getCurrency()->getName()
        ]);
    }
    
    /**
     * 
     * @param Product $product
     * @return array
     */
    private function normalizeProduct(Product $product): array
    {
        return [
            'product' => [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'cartPrice' => $this->currency->getCartPrice($product)
            ],
            'qty' => $this->cart[$product->getId()]            
        ];
    }
    
    /**
     * @Route("", methods={"GET"})
     */
    public function cart(CartConverter $converter): JsonResponse
    {
        $products = array_map(
            [$this, 'normalizeProduct'], 
            $converter->getProducts()
        );
        
        return $this->json([
            'products' => $products,
            'currency' => $this->currency->getCurrency()->getName()
        ]);
    }
    
    /**
     * @param Request $request
     * @Route("", methods={"POST"})
     */
    public function push(Request $request)
    {
        $id = (int)$request->request->get('id');
        $product = $this->productsRepo->find($id);
        
        if(!$product || !$product->isActive()) {
            throw new BadRequestException('Product not found');
        }
        
        if((int)$this->cart[$id] < Cart::MAX_CAPACITY) {
            $this->cart[$id] = (int)$this->cart[$id] + 1;
        }
        
        return $this->json($this->normalizeProduct($product));
    }
    
    /**
     * @Route("", methods={"PATCH"})
     */
    public function update(Request $request)
    {
        $id = (int)$request->request->get('id');
        $qty = (int)$request->get('qty');
        /* @var $product Product */
        $product = $this->productsRepo->find($id);
        
        if(!$product || !$product->isActive()) {
            throw new BadRequestException('Product not found');
        }
        
        if($qty < 0) {
            throw new BadRequestException('Quantity must be positive');
        }
        
        $this->cart[$id] = $qty;
        
        return $this->json($this->normalizeProduct($product));
    }
    
}
