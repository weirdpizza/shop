<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use App\Entity\{DeliveryAddress, Order};
use App\Services\{Cart, CartConverter, CurrencyService};


class CheckoutController extends AbstractController
{
    private EntityManagerInterface $em;
    
    private Cart $cart;
    
    private CartConverter $converter;
    
    public function __construct(
        EntityManagerInterface $em,
        Cart $cart,
        CartConverter $converter
    )
    {
        $this->em = $em;
        $this->cart = $cart;
        $this->converter = $converter;
    }
    
    private function setAddress(Order $order, ?int $addressId, ?string $address)
    {   
        /* @var $customer \App\Entity\Customer */
        $customer = $this->getUser();
        
        $order->setCustomer($customer);
        
        if($this->isGranted('IS_AUTHENTICATED_REMEMBERED') && $addressId) {
            
            $deliveryAddress = $this->em
                ->getRepository(DeliveryAddress::class)
                ->findOneByIdAndUser($addressId, $customer);
            
            if(!$deliveryAddress) {
                throw new BadRequestException('Wrong address id');
            }
            
        } elseif ($address) {
            $deliveryAddress = new DeliveryAddress();
            $deliveryAddress->setAddress($address);
            
            if($customer) {
                $customer->addDeliveryAddress($deliveryAddress);
            }
        } else {
            throw new BadRequestException('Delivery address required');
        }

        $order->setDeliveryAddress($deliveryAddress);
    }
    
    private function setCost(Order $order): void
    {
        $deliveryFee = (float)$this->getParameter('app.delivery_fee');
        $sum = 0;
        foreach($order->getOrderItems() as $item) {
            /* @var $item \App\Entity\OrderItem */
            $sum += $item->getPrice();
        }
        $order->setCost($sum + $deliveryFee);   
    }


    private function createOrder(array $form)
    {
        $items = $form['items'];

        if(empty($items)) {
            return $this->redirectToRoute('checkout');
        }

        $this->cart->clean();

        foreach($items as $item) {
            $id = (int)$item['id'];
            $qty = (int)$item['qty'];
            $this->cart[$id] = $qty;
        }
        
        $order = $this->converter->toOrder();
        $customer = $this->getUser();
        
        if($customer) {
            $order->setCustomer($customer);
        }
        
        $addressId = $form['address_id'] ?? null;
        $address = $form['address'] ?? null;
        $this->setAddress($order, $addressId, $address);
        $this->setCost($order);

        return $order;
    }
    
    /**
     * @Route("/checkout", methods={"POST"})
     */
    public function checkout(Request $request, CsrfTokenManagerInterface $tokenManager)
    {
        if($this->cart->isEmpty()) {
            return $this->redirectToRoute('default');
        }
        
        $token = $request->request->get('_token');
        if(!$this->isCsrfTokenValid('checkout', $token)) {
            $this->addFlash('checkout', 'Token expired');
            return $this->redirectToRoute('checkout');
        }
        
        $tokenManager->refreshToken('checkout');
        
        $form = $request->get('checkout');
        $order = $this->createOrder($form);
        $this->em->persist($order);
        $this->em->flush();
        
        $this->cart->clean();
        
        // TODO: dispatchMessage
        
        return $this->render('checkout/success.html.twig', [
            'order' => $order
        ]);
    }
    
    /**
     * @Route("/checkout", name="checkout")
     */
    public function index(CurrencyService $currencyService)
    {
        if($this->cart->isEmpty()) {
            return $this->redirectToRoute('default');
        }

        $order = $this->converter->toOrder();
        
        $addresses = null;
        $customer = $this->getUser();
        if($customer) {
            $addresses = $this->em->getRepository(DeliveryAddress::class)
                ->findByCustomer($customer);
        }
        
        $currency = $currencyService->getCurrency();
        $deliveryFee = (float)$this->getParameter('app.delivery_fee') * $currency->getRate();
        
        return $this->render('checkout/index.html.twig', [
            'sum' => $this->converter->calculateSum() * $currency->getRate(),
            'order' => $order,
            'addresses' => $addresses,
            'currency' => $currency->getName(),
            'deliveryFee' => $deliveryFee
        ]);
    }
}
