<?php

namespace App\Repository;

use App\Entity\{DeliveryAddress, Customer};
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DeliveryAddress|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeliveryAddress|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeliveryAddress[]    findAll()
 * @method DeliveryAddress[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeliveryAddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeliveryAddress::class);
    }

    public function findOneByIdAndUser(int $id, Customer $customer): ?DeliveryAddress
    {
        $address = $this->createQueryBuilder('address')
            ->where('address.id = :id')
            ->andWhere('address.customer = :customer')
            ->setParameter(':id', $id)
            ->setParameter(':customer', $customer)
            ->getQuery()
            ->getOneOrNullResult();
        
        return $address;
    }
    
    public function findByCustomer(Customer $customer)
    {
        $addresses = $this->createQueryBuilder('address')
            ->where('address.customer = :customer')
            ->orderBy('address.isDefault')
            ->setParameter(':customer', $customer)
            ->getQuery()
            ->getResult();
        
        return $addresses;
    }
    
}
