<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Product;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findActive(): ?array
    {
        return $this->createQueryBuilder('product')
            ->where('product.active = true')
            ->orderBy('product.rank', 'DESC')
            ->getQuery()
            ->getResult();
    }
    
    public function findActiveById($ids): array
    {
        return $this->createQueryBuilder('product')
            ->where('product.active = true')
            ->andWhere('product.id IN (:products)')
            ->setParameter(':products', $ids)
            ->indexBy('product', 'product.id')
            ->getQuery()
            ->getResult();
    }
    
}
