<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Currency;

class CurrencyFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $usd = new Currency('USD', 1.0);
        $manager->persist($usd);
        
        $eur = new Currency('EUR', 0.85);
        $manager->persist($eur);
        
        $manager->flush();
    }
}
