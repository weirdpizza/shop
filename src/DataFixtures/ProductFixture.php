<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Product;

class ProductFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $pepperoni = new Product();
        $pepperoni->setName('Pepperoni')
            ->setActive(false)
            ->setPrice(10.0)
            ->setDiscount(9.99)
            ->setDescription('Just an ordinary Pepperoni pizza. Yeah, we make these too')
            ->setPicture('https://placekitten.com/256/256');

        $manager->persist($pepperoni);
        
        $borscht = new Product();
        $borscht->setName('Borscht Pizza')
            ->setActive(false)
            ->setPrice(11.0)
            ->setDescription(
                'Pork, beef, bell peppers, onion, and our special '
                . 'tomato-beets-cabbage sauce. Sourcream included.'
            )
            ->setPicture('https://placekitten.com/256/256');

        $manager->persist($borscht);

        $pieces = new Product();
        $pieces->setName('Pieces')
            ->setActive(false)
            ->setPrice(8.0)
            ->setDescription('Just random leftovers from our fridge')
            ->setPicture('https://placekitten.com/256/256');
        $manager->persist($pieces);
        
        $descriptions = [
            'Maecenas ultrices ligula a fermentum bibendum.',
            'Proin pharetra erat id maximus elementum.',
            'Suspendisse consectetur dolor vitae risus interdum, nec eleifend purus sagittis.',
            'Etiam sit amet est scelerisque, congue quam id, aliquam ipsum.',
            'Etiam non nunc ut urna tempus sodales.',
            'Curabitur quis ipsum aliquam, finibus mauris eget, molestie justo.',
            'Maecenas in tortor eu dui accumsan semper sed quis nulla.',
            'Pellentesque venenatis nibh at ultricies ullamcorper.'
        ];
        
        for($i = 1; $i <= 8; $i++) {
            $pizza = new Product();
            $price = 10 + (rand(1, 10) > 5 ? -1 : 1) * (rand(1, 20) / 10);
            $discount = (rand(1, 10) > 5 ? $price / 2 : 0);
            $pizza->setName("Boring pizza #$i")
                ->setActive(true)
                ->setPrice($price)
                ->setDiscount($discount)
                ->setPicture("/images/pizza$i.png")
                ->setDescription($descriptions[$i-1]);
            
            $manager->persist($pizza);
        }
        
        $manager->flush();
    }
}
