<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;

/**
 * @ORM\Entity
 * @ORM\Table(indexes={@ORM\Index(name="currency_name_idx", columns={"name"})})
  */
class Currency implements TimestampableInterface
{
    use TimestampableTrait;
     
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $rate = 1.0;

    public function __construct(?string $name = null, ?float $rate = null)
    {
        $this->name = $name;
        $this->rate = $rate;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRate(): ?float
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }
}
