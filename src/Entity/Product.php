<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Knp\DoctrineBehaviors\Model\Uuidable\UuidableTrait;
use Knp\DoctrineBehaviors\Contract\Entity\UuidableInterface;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\Table(indexes={
 *  @ORM\Index(name="product_active_idx", columns={"active"}),
 *  @ORM\Index(name="product_rank_idx", columns={"rank"})
 * })
 */
class Product implements UuidableInterface
{
    use UuidableTrait;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"Cart"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     * @Groups({"Cart"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"Default"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"Cart"})
     */
    private $picture;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"Default"})
     */
    private $isHot;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"Default"})
     */
    private $isVegi;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"Default"})
     */
    private $active;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @Groups({"Default"})
     */
    private $rank;

    /**
     * @ORM\Column(type="bigint")
     * @Groups({"Cart"})
     */
    private $price;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     * @Groups({"Cart"})
     */
    private $discount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getIsHot(): ?bool
    {
        return $this->isHot;
    }

    public function setIsHot(?bool $isHot): self
    {
        $this->isHot = $isHot;

        return $this;
    }

    public function getIsVegi(): ?bool
    {
        return $this->isVegi;
    }

    public function setIsVegi(?bool $isVegi): self
    {
        $this->isVegi = $isVegi;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getPrice(): float
    {
        return (int)$this->price / 100;
    }

    public function setPrice(float $price): self
    {
        $this->price = (int)round($price * 100);

        return $this;
    }

    public function getDiscount(): float
    {
        return (int)$this->discount / 100;
    }

    public function setDiscount(?float $discount): self
    {
        $this->discount = (int)round($discount * 100);

        return $this;
    }
    
    /**
     * @Groups({"Cart"})
     * @return float
     */
    public function getCartPrice(): float
    {
        return $this->discount ? $this->getDiscount() : $this->getPrice();
    }
}
