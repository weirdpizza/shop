<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use App\Services\CurrencyService;

class PriceExtension extends AbstractExtension
{
    private CurrencyService $currency;
    
    public function __construct(CurrencyService $currency)
    {
        $this->currency = $currency;
    }
    
    public function getFilters()
    {
        return [
            new TwigFilter('price', [$this->currency, 'getPrice']),
            new TwigFilter('discount', [$this->currency, 'getDiscount']),
            new TwigFilter('cart_price', [$this->currency, 'getCartPrice'])
        ];
    }
    
}
