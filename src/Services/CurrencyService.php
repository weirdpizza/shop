<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\{Currency, Product};

class CurrencyService
{

    private ?Currency $currency;
    
    /**
     * 
     * @param string $default
     * @param EntityManagerInterface $em
     * @param SessionInterface $session
     */
    public function __construct(
        string $default,
        EntityManagerInterface $em,
        SessionInterface $session
    )
    {
        $currencyName = $session->get('currency', $default);
        $this->currency = $em->getRepository(Currency::class)
            ->findOneByName($currencyName);
    }
    
    /**
     * 
     * @param Product $product
     * @return float
     */
    public function getCartPrice(Product $product): float
    {
        return $this->currency->getRate() * $product->getCartPrice();
    }
    
    /**
     * 
     * @param Product $product
     * @return type
     */
    public function getPrice(Product $product): float
    {
        return $this->currency->getRate() * $product->getPrice();
    }
    
    /**
     * 
     * @param Product $product
     * @return type
     */
    public function getDiscount(Product $product): float
    {
        return $this->currency->getRate() * $product->getDiscount();
    }
    
    /**
     * 
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }
}
