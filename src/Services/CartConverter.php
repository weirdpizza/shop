<?php

namespace App\Services;

use App\Services\Cart;
use App\Repository\ProductRepository;
use App\Entity\{Product, Order, OrderItem};

class CartConverter
{
    
    private Cart $cart;
    
    private ProductRepository $repo;
    
    private CurrencyService $currency;
    
    public function __construct(
        Cart $cart,
        ProductRepository $repo,
        CurrencyService $currency
    )
    {
        $this->cart = $cart;
        $this->repo = $repo;
        $this->currency = $currency;
    }
    
    
    /**
     * 
     * @return array
     */
    public function getProducts(): array
    {
        $ids = $this->cart->getIds();

        if(!empty($ids)) {
            $products = $this->repo->findActiveById($ids);
        } else {
            $products = [];
        }
        
        return $products;
    }
    
    /**
     * 
     * @return float
     */
    public function calculateSum(): float
    {
        $products = $this->getProducts();
        $total = 0;
        
        foreach($this->cart as $id => $qty) {
            if(isset($products[$id])) {
                /* @var $product Product */
                $product = $products[$id];
                $total += $product->getCartPrice() * $qty;
            }
        }
        
        return $total;
    }

    /**
     * 
     * @return Order
     */
    public function toOrder(): Order
    {
        $order = new Order();
        $products = $this->getProducts();
        
        foreach($products as $product) {
            $qty = $this->cart[$product->getId()];
            $orderItem = new OrderItem($product, $qty);
            $order->addOrderItem($orderItem);
        }
        
        return $order;
    }
}
