<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Cart implements \Iterator, \ArrayAccess
{
    const SESSION_KEY = 'cart';

    const MAX_CAPACITY = 10;
    
    private SessionInterface $session;
    
    private array $cart = [];
    
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
        $this->cart = $this->getData();
    }
    
    /**
     * Load the cart from the session
     * @return array
     */
    private function getData(): array
    {
        if($this->session->has(self::SESSION_KEY)) {
            $rawData = $this->session->get(self::SESSION_KEY);
            $cart = json_decode($rawData, true);
        } else {
            $cart = [];
        }
        return $cart;
    }
    
    /**
     * Serialize and save the cart to the session
     * @return void
     */
    private function saveData(): void
    {
        $data = json_encode($this->cart);
        $this->session->set(self::SESSION_KEY, $data);
    }

    /**
     * Get ids of products in the cart
     * @return array
     */
    public function getIds(): array
    {
        return array_keys($this->cart);
    }
    
    /**
     * Throw everything away
     * @return void
     */
    public function clean(): void
    {
        $this->cart = [];
        $this->saveData();
    }
    
    public function isEmpty(): bool
    {
        return empty($this->cart);
    }
    
    // ArrayAccess
    
    /**
     * 
     * @param int $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return isset($this->cart[$offset]);
    }

    /**
     * 
     * @param int $offset
     * @return ?int
     */
    public function offsetGet($offset)
    {
        return $this->cart[$offset] ?? null;
    }

    /**
     * 
     * @param int $offset
     * @param int $value
     * @return void
     * @throws RangeException
     */
    public function offsetSet($offset, $value): void
    {
        if($value < 0) {
            throw new \RangeException('Trying to set negative amount of items in the cart');
        }
        
        if($value > 0) {
            $this->cart[$offset] = $value;
        } else {
            unset($this->cart[$offset]);
        }
        
        $this->saveData();
    }

    /**
     * 
     * @param int $offset
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->cart[$offset]);
        $this->saveData();
    }

    // Iterator
    
    public function current()
    {
        return current($this->cart);
    }

    public function key()
    {
        return key($this->cart);
    }

    public function next(): void
    {
        next($this->cart);
    }

    public function rewind(): void
    {
        reset($this->cart);
    }

    public function valid(): bool
    {
        return $this->key() && $this->offsetExists($this->key());
    }

    
}
