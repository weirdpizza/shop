FROM alpine:edge

RUN apk add php7 php7-session php7-pdo php7-pdo_mysql php7-ctype php7-phar \
    php7-dom php7-simplexml php7-xml php7-tokenizer php7-iconv \
    php7-intl php7-json php7-mbstring php7-openssl

WORKDIR /var/www

COPY docker-entrypoint.sh .

# composer
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q | php -- --quiet

ENTRYPOINT ["/var/www/docker-entrypoint.sh"]
